MediaScannerConnectionPlugin
============

This plugin invokes the Media Scanner Connection on Android OS to enable the device to trigger the indexing of new media files downloaded to the device. The triggering will enable the media files to show up in the Android Gallery.

Installation
------------

### For Cordova 3.0.x:

1. To add this plugin just type: `cordova plugin add https://ailuromaniac@bitbucket.org/ailuromaniac/mediascannerconnectionplugin.git`
2. To remove this plugin type: `cordova plugin remove org.bitbucket.ailuromaniac.cordova.plugin.MediaScannerConnectionPlugin`


Usage:
------

Call the `window.MediaScannerConnectionPlugin(successCallback, failureCallback, newFilePath)` method by passing in your success and error callbacks:

### Example
```javascript
document.addEventListener('deviceready', function () {
{
	var newFilePath = '/new/file/path';
	window.MediaScannerConnectionPlugin.scanFile(
        function(msg){
            console.log(msg);
        },
        function(err){
            console.log(err);
        },
	newFilePath
    );
}
```

The function will call your successCallback upon success, and failureCallback on failure.

## Special Thanks
Peter Gao (http://github.com/peteygao) for developing MediaScannerPlugin