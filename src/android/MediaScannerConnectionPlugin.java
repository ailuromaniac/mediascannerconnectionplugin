package org.bitbucket.ailuromaniac.cordova.plugin;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.media.MediaScannerConnection;
import android.net.Uri;

import java.lang.Exception;

/**
 * This class scans a folder for new media files
 * @author: Aneesa Awaludin
 * @date: 2 Oct 2015
 */
public class MediaScannerConnectionPlugin extends CordovaPlugin {
    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        if (action.equals("scanFile")) {
          String newPath = args.getString(0);

          // ContentValues values = new ContentValues();
          // values.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
          // values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg"); // setar isso
          // getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

          try {
            MediaScannerConnection.scanFile(
              this.cordova.getActivity().getApplicationContext(),
              new String[] {newPath},
              null,
              new MediaScannerConnection.OnScanCompletedListener() {
                public void onScanCompleted(String path, Uri uri){
                  if(uri==null){
                    callbackContext.error("Scanning FAILED for new media file " + path);
                  }
                  else {
                    callbackContext.success("Scanned a new media file " + path + ", URI " + uri);
                  }
                }
              }
            );
          } catch (Exception e) {
            callbackContext.error("Exception caught " + e.getMessage());
          }
          return true;
        }
        return false;
    }
}
