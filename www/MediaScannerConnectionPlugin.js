//
//  MediaScannerPlugin.js
//  MediaScannerPlugin PhoneGap/Cordova plugin
//
//  Created by Peter Gao on 23/09/2013.
//  Copyright (c) 2012 Peter Gao. All rights reserved.
//  MIT Licensed
//

module.exports = {

    scanFile:function(successCallback, failureCallback, newFilePath) {
        // successCallback required
        if (typeof successCallback !== "function") {
            console.log("MediaScannerConnectionPlugin Error: successCallback is not a function");
        }
        else if (typeof failureCallback !== "function") {
            console.log("MediaScannerConnectionPlugin Error: failureCallback is not a function");
        }
        else {
            return cordova.exec(successCallback, failureCallback, "MediaScannerConnectionPlugin", "scanFile", [newFilePath]);
        }
    }
};
